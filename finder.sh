#!/bin/bash

NEWLOC=`curl -L "https://www.devontechnologies.com/apps/freeware" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .zip | head -1 `

if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi
